package rznu;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


@Controller
@RequestMapping("/api")
public class PublicController {

    // users with IDs: 1, 2
    public static String msgForUsr1;
    public static String msgForUsr2;

    @GetMapping(path = "/{usrid}/p_chat")
    public String getChatPoll(@PathVariable int usrid) {
        if (usrid == 1)
            return "cl1poll";
        else
            return "cl2poll";
    }

    @GetMapping(path = "/{usrid}/lp_chat")
    public String getChatLongPoll(@PathVariable int usrid) {
        if (usrid == 1)
            return "cl1longpoll";
        else
            return "cl2longpoll";
    }


    @GetMapping(path = "/{usrid}/ws_chat")
    public String getChatWebsocket(@PathVariable int usrid) {
        if (usrid == 1)
            return "cl1ws";
        else
            return "cl2ws";
    }

    @GetMapping(path = "/{usrid}/index")
    public String getIndexPage(@PathVariable int usrid) {
        if (usrid == 1)
            return "index1";
        else
            return "index2";
    }


}
