package rznu;

import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.AbstractWebSocketHandler;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class WebSocketHandler extends AbstractWebSocketHandler {

    static List<WebSocketSession> wsSessions = new CopyOnWriteArrayList<>();

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        wsSessions.add(session);

        int usrid = Integer.parseInt(session.getUri().getPath().substring(1, 2));
        consumeMessage(usrid);
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) {
        wsSessions.remove(session);
    }

    public static void consumeMessage(int curUserIdConsume) throws Exception {

        boolean isUserIdOneConsume = curUserIdConsume == 1;
        for (WebSocketSession sessionElem : wsSessions) {
            if (Integer.parseInt(sessionElem.getUri().getPath().substring(1, 2)) == curUserIdConsume) {
                if (isUserIdOneConsume) {
                    if (PublicController.msgForUsr1 != null) {
                        ChatController.lastMessageForUsr1 = PublicController.msgForUsr1;
                        PublicController.msgForUsr1 = null;
                    }

                    sessionElem.sendMessage(new TextMessage(ChatController.lastMessageForUsr1));
                } else {
                    if (PublicController.msgForUsr2 != null) {
                        ChatController.lastMessageForUsr2 = PublicController.msgForUsr2;
                        PublicController.msgForUsr2 = null;
                    }
                    sessionElem.sendMessage(new TextMessage(ChatController.lastMessageForUsr2));
                }

            }
        }


    }


    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {

        int curUserId = Integer.parseInt(session.getUri().getPath().substring(1, 2));
        String msgtxt = message.getPayload();
        switch (curUserId) {
            case 1:
                PublicController.msgForUsr2 = msgtxt;
                break;
            case 2:
                PublicController.msgForUsr1 = msgtxt;
                break;
        }

        ChatController.consumeMessageLongPolling(curUserId);

        for (WebSocketSession sessionElem : wsSessions) {
            if (!sessionElem.equals(session)) {
                consumeMessage(Integer.parseInt(sessionElem.getUri().getPath().substring(1, 2)));
            }

        }

    }


}
