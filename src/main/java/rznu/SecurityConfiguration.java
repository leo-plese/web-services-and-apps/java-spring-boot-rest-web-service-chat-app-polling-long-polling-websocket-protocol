package rznu;

import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;


@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors().and()
                .csrf().disable()
                .authorizeRequests()
                // sljedeca linija se kopira za svaku neautenticiranu metodu
                .antMatchers(HttpMethod.GET, "/api/public").permitAll()
                .antMatchers("/api/*/index").permitAll()
                .antMatchers("/*/p_my_message").permitAll()
                .antMatchers("/*/lp_my_message").permitAll()
                .antMatchers("/*/lp_my_old_message").permitAll()
                .antMatchers("/*/p_message").permitAll()
                .antMatchers("/*/ws_message").permitAll()
                .antMatchers("/*/lp_message").permitAll()
                .antMatchers("/api/*/p_chat").permitAll()
                .antMatchers("/api/*/lp_chat").permitAll()
                .antMatchers("/api/*/ws_chat").permitAll()
                .anyRequest().authenticated()
                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }

    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", new CorsConfiguration().applyPermitDefaultValues());

        return source;
    }
}
