package rznu;

import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.async.DeferredResult;


@RestController       // for: polling, long polling
@RequestMapping("/")
public class ChatController {

    // *********** LONG POLLING***********
    private static DeferredResult<String> chatRequest1 = null, chatRequest2 = null;
    public static String lastMessageForUsr1 = "New message appears here...", lastMessageForUsr2 = "New message appears here...";

    //    // usrid - ID of user who wants to get his messages
    @GetMapping(path = "/{usrid}/lp_my_message")
    public DeferredResult<String> getMessageLongPoll(@PathVariable int usrid) {
        boolean isUserIdOne = usrid == 1;
        String msgForUsr = isUserIdOne ? PublicController.msgForUsr1 : PublicController.msgForUsr2;
        String lastMsgForUsr = isUserIdOne ? lastMessageForUsr1 : lastMessageForUsr2;

        DeferredResult<String> deferredResult = new DeferredResult<String>(10000L, lastMsgForUsr);

        if (isUserIdOne) {
            chatRequest1 = deferredResult;
            System.out.println("GET usr 1");
        } else {
            chatRequest2 = deferredResult;
            System.out.println("GET usr 2");
        }

        deferredResult.onCompletion(new Runnable() {
            @Override
            public void run() {
                if (isUserIdOne) {
                    chatRequest1 = null;
                    System.out.println("GET usr 1 -- end");
                } else {
                    chatRequest2 = null;
                    System.out.println("GET usr 2 -- end");
                }
            }
        });

        if (msgForUsr != null) {
            System.out.println(usrid + " MSG! " + msgForUsr);
            System.out.println(PublicController.msgForUsr1 + " : " + PublicController.msgForUsr2);
            if (isUserIdOne)
                PublicController.msgForUsr1 = null;
            else
                PublicController.msgForUsr2 = null;


            deferredResult.setResult(msgForUsr);
        }

        return deferredResult;
    }

    @GetMapping(path = "/{usrid}/lp_my_old_message")
    public String getOldMessageLongPoll(@PathVariable int usrid) {
        if (usrid == 1) {
            return ChatController.lastMessageForUsr1;
        } else {
            return ChatController.lastMessageForUsr2;
        }
    }

    // usrid - ID of user who posts a message
    @PostMapping(path = "/{usrid}/lp_message")
    public String postMessageLongPoll(@PathVariable int usrid, @RequestBody String msgtxt) throws Exception {
        if (usrid == 1) {
            PublicController.msgForUsr2 = msgtxt;
            System.out.println("POST usr 1");
            WebSocketHandler.consumeMessage(2);
        } else {
            PublicController.msgForUsr1 = msgtxt;
            System.out.println("POST usr 2");
            WebSocketHandler.consumeMessage(1);
        }

        System.out.println(PublicController.msgForUsr1 + " : " + PublicController.msgForUsr2);

        consumeMessageLongPolling(usrid);

        return "'" + msgtxt + "'";
    }

    public static void consumeMessageLongPolling(int usrid) {
        boolean isUserIdOne = usrid == 1;

        if (isUserIdOne) {
            if (chatRequest2 != null)
                chatRequest2.setResult(PublicController.msgForUsr2);
            if (PublicController.msgForUsr2 != null)
                lastMessageForUsr2 = PublicController.msgForUsr2;
        } else {
            if (chatRequest1 != null)
                chatRequest1.setResult(PublicController.msgForUsr1);
            if (PublicController.msgForUsr1 != null)
                lastMessageForUsr1 = PublicController.msgForUsr1;
        }

    }


    // *********** POLLING***********
    //    // usrid - ID of user who wants to get his messages
    @GetMapping(path = "/{usrid}/p_my_message")
    public String getMessagePoll(@PathVariable int usrid) {
        boolean isUserIdOne = usrid == 1;
        String msgForUsr = isUserIdOne ? PublicController.msgForUsr1 : PublicController.msgForUsr2;

        if (msgForUsr != null) {
            System.out.println(usrid + " MSG! " + msgForUsr);
            System.out.println(PublicController.msgForUsr1 + " : " + PublicController.msgForUsr2);
            if (isUserIdOne)
                PublicController.msgForUsr1 = null;
            else
                PublicController.msgForUsr2 = null;
            return msgForUsr;
        } else {
            return "There is no message for you.";
        }
    }

    // usrid - ID of user who posts a message
    @PostMapping(path = "/{usrid}/p_message")
    public String postMessagePoll(@PathVariable int usrid, @RequestBody String msgtxt) throws Exception {


        if (usrid == 1) {
            PublicController.msgForUsr2 = msgtxt;
            WebSocketHandler.consumeMessage(2);
        } else {
            PublicController.msgForUsr1 = msgtxt;
            WebSocketHandler.consumeMessage(1);
        }


        consumeMessageLongPolling(usrid);

        return "'" + msgtxt + "'";
    }

}
