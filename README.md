# Java Spring Boot REST Web Service Chat App Polling Long Polling WebSocket Protocol

Implemented in Java using Spring Boot libraries. Webpages are in HTML.

Maven project has dependencies in pom.xml.

Web service is aimed to serve a proxy server and two clients. Each client can, independently of the other client, choose one of the following communication techniques: Polling, Long Polling or WebSocket protocol.

My lab assignment in Service-Oriented Computing, FER, Zagreb.

Task description in "TaskSpecification.pdf".

Created: 2021
